\begin{wrapfigure}[16]{r}{0.4\linewidth}
\centering
\def\svgwidth{6.5cm}
\import{book/pictures/}{Derivative_of_a_function}
%\includesvg[width=6.5cm]{Derivative_of_a_function}
\caption{Иллюстрация термина производной}\label{fig-mkr-derivative}
\end{wrapfigure}
Как мы прошли в разделе \ref{ch1_equations}, решение задачи расчета электромагнитного поля основывается на решении уравнений Лапласа и Пуассона \eqref{eq-eqfield-Poisson_el}-\eqref{eq-eqfield-Laplace-m}, записанных в частных производных потенциала поля (электрического потенциала или векторного магнитного потенциала). Рассмотрим наиболее простой метод численного решения этих уравнений. В литературе его также называют метод сеток. Как следует из первого названия он основан на переходе от производных к конечным разностям. Как следует из второго названия такой переход выполняется на сетке, которая покрывает область задачи.

Для начала разберемся, что это значит. Производная функции в точке $ x_0 $ равна пределу
\begin{equation}
f^\prime(x_0)=\lim\limits_{x \to x_0} \dfrac{f(x)-f(x_0)}{x-x_0}.
\end{equation}

Иллюстрация этого предела показана на рис. \ref{fig-mkr-derivative}. С уменьшением $ \Delta x $ линия, соединяющая точки $ (x_0,f(x_0)) $ и $ (x,f(x)) $, все больше приближается к касательной кривой в точке $ x_0 $ и уже сама производная равна тангенсу угла наклона касательной. Чем меньше $ \Delta x $ тем больше линия приближается к касательной функции в точке $ x_0 $.
Запись производной в конечных разностях и представляет такое приближение к прозводной, когда разница $ f(x)-f(x_0) $ и $ x-x_0 $ достаточно малы, но конечны. При этом выражение $ \dfrac{f(x)-f(x_0)}{x-x_0} $ приближенно равно производной $ f^\prime(x_0) $.
Таким образом, производная функции в форме конечных разностей выглядит как
\begin{equation}
 \frac{d f}{d x} \approx \frac{\Delta f}{\Delta x}.
\end{equation}

Представим уравнения Лапласа и Пуассона в форме конечных разностей. В исходной форме они даны в \eqref{eq-eqfield-Laplace-Poisson}. Вспомним, что для функции многих переменных применяются частные производные
\begin{equation}
\dpartfrac{f(x,y,z)}{x}=\lim\limits_{\Delta x \to 0} \dfrac{f(x + \Delta x, y,z)-f(x,y,z)}{\Delta x}.
\end{equation}

Тогда
\begin{align}
\dpartfrac{\gls{P}}{x} &\approx \dfrac{\gls{P}(x+\Delta x,y,z)-\gls{P}(x,y,z)}{\Delta x}, \\[1em]
\dpartfrac{\gls{P}}{y} &\approx \dfrac{\gls{P}(x,y+\Delta y,z)-\gls{P}(x,y,z)}{\Delta y}, \\[1em]
\dpartfrac{\gls{P}}{z} &\approx \dfrac{\gls{P}(x,y,z+\Delta z)-\gls{P}(x,y,z)}{\Delta z}.
\end{align} 
Для иллюстрации этого подхода рассмотрим двухмерное поле, область расчета которого равномерно покрыта сеткой с равным шагом $ \gls{h} $. Величину $ \gls{h} $ называют шагом дискретизации. \newsym{h}{\ensuremath{h}}{шаг дискретизации}{false} Обозначим произвольную точку на сетке $ i,j $ (рис. \ref{fig-finite_diff1}). Частные производные потенциала в форме конечных разностей для этой точки будут приближенно равны 
\begin{align}
\dpartfrac{\gls{P}}{x} &\approx \dfrac{\gls{P}_{i+1,j}-\gls{P}_{i,j}}{h}, \\[1em]
\dpartfrac{\gls{P}}{y} &\approx \dfrac{\gls{P}_{i,j+1}-\gls{P}_{i,j}}{h}.
\end{align}

\begin{figure}[b]
\centering
 \begin{tikzpicture}[scale=1.5]
 \clip (-0.7,-0.3) rectangle (4.3,2.7);
 \draw[thick] (-1,-1) grid (5,3);
 %\fill (1,0) circle (2pt) node[above right] {} $ i,j-1 $};
 \fill (1,1) circle (2pt) node[above right] { $ i,j $};
 %\fill (0,1) circle (2pt) node[above right] { $ i-1,j $};
 \fill (1,2) circle (2pt) node[above right] { $ i,j+1 $};
 \fill (2,1) circle (2pt) node[above right] { $ i+1,j $};
 \draw[<->,>=stealth',thick] (-0.2,1) -- node[left] {\small $ h $}(-0.2,2);
 \draw[<->,>=stealth',thick] (0,2.2) -- node[above] {\small $ h $}(1,2.2);
 \end{tikzpicture}
\caption{Сетка в области расчета и точки для расчета частных производных  потенциала \gls{P}}\label{fig-finite_diff1}
\end{figure}

Идем далее и применим то же правило для частной производной второго порядка. 