Задачи расчета и моделирования электромагнитного поля многообразны. Для того, чтобы компьютер мог их решать хорошо было бы найти какой-нибудь один универсальный подход. Как можно объединить решение для расчета электростатического поля двух точечных зарядов и плоского конденсатора, расчет магнитного поля постоянного магнита буквой <<U>> и витка с током? А между тем, он есть. И данный параграф посвящен универсальным уравнениям, подходящим для решения всего многообразия задач расчета и моделирования электромагнитного поля.

Из курса физики хорошо известна теорема Гаусса, которая устанавливает связь между потоком напряженности электрического поля сквозь произвольную замкнутую поверхность и зарядом в объеме, заключенном в этом объеме:
\begin{equation}\label{eq-eqfield-tgauss}
\oint\limits_s \vec{\gls{E}} d\vec s = \frac{\gls{q}}{\gls{epsr}\gls{eps0}},
\end{equation}
где 
\newsym{E}{\ensuremath{E}}{напряженность электрического поля, В/м}{true}, 
\newsym{q}{\ensuremath{q}}{электрический заряд, Кл}{true}, 
\newsym[z]{epsr}{\ensuremath{\eps_r}}{относительная диэлектрическая проницаемость}{true}, 
\newsym[z]{eps0}{\ensuremath{\eps_0}}{электрическая постоянная, равная $ 1/(4\pi c^2 \cdot 10^{-7})=8.85\cdot 10^{-12} $ Ф/м}{true}.

Теорема Гаусса справедлива для напряженности электрического поля в однородной среде. Теорема Гаусса справедлива и для других полей, в которых верен аналог закона Кулона, например, магнитного и гравитационного. Для магнитного поля теорема Гаусса звучит следующим образом: поток вектора магнитной индукции через любую замкнутую поверхность равен нулю.
\begin{equation}
\gls{upphi} = \oint\limits_s \vec{\gls{B}} d\vec s = 0,
\end{equation}
где \newsym[z]{upphi}{\ensuremath{\upphi}}{магнитный поток, Вб}{true},
\newsym{B}{\ensuremath{B}}{магнитная индукция, Тл}{true}.

Представим, что рассматриваемый объем очень и очень мал, даже более того, он стремится к нулю. Это позволит рассматривать уравнение \refeq{eq-eqfield-tgauss} в точке. Приближая рассматриваемый объем к точке, поток сквозь замкнутую поверхность и заряд $ \Delta \gls{q} $ также будут приближаться к нулю. Разделим обе части уравнения на объем $ \Delta V \to 0 $
\begin{equation}
\lim\limits_{\Delta V \to 0} \dfrac{\oint\limits_s \vec{\gls{E}} d\vec s}{\Delta V} = \lim\limits_{\Delta V \to 0} \frac {\Delta \gls{q}}{\gls{epsr}\gls{eps0} \Delta V}.
\end{equation}

В левой части уравнения стоит дивергенция вектора $ \vec{\gls{E}} $, а в правой части получаем объемную плотность заряда \gls{rho}.  Дивергенция обозначается $ \mathrm{div} \vec{\gls{E}}  $ и теорема Гаусса для точки (или обычно говорят в дифференциальной форме) принимает вид
\begin{equation}\label{eq-eqfield-divE_eq_rho}
\mathrm{div} \vec{\gls{E}} = \frac {\gls{rho}} {\gls{epsr}\gls{eps0}}.
\end{equation}
\newsym[z]{rho}{\ensuremath{\rho}}{объемная плотность электрического заряда, Кл/м$ ^3 $}{false}
Дивергенцию также обозначают греческой буквой набла $ \nabla $. Для декартовых координат она равна
\begin{multline}\label{eq-eqfield-divE}
\mathrm{div} \vec{\gls{E}} =\nabla \vec{\gls{E}} = \left(\vec i \dpartfrac{}{x} + \vec j \dpartfrac{}{y} + \vec k \dpartfrac{}{z} \right) \vec{\gls{E}}  = \left(\vec i \dpartfrac{}{x} + \vec j \dpartfrac{}{y} + \vec k \dpartfrac{}{z} \right)(\vec i \gls{E}_x + \vec j \gls{E}_y + \vec k \gls{E}_z) = \\
=\dpartfrac{\gls{E}_x}{x} +  \dpartfrac{\gls{E}_y}{y} +  \dpartfrac{\gls{E}_z}{z}.
\end{multline}

Известно также, что $ \vec{\gls{E}} = - \grad\gls{Phi}  $ , т.е. 
\begin{equation}\label{eq-eqfield-E_eq_gradPhi}
\gls{E}_x = -\dpartfrac{\gls{Phi}}{x}, \quad \gls{E}_y = -\dpartfrac{\gls{Phi}}{y}, \quad \gls{E}_z = -\dpartfrac{\gls{Phi}}{z}.
\end{equation}
\newsym[z]{Phi}{\ensuremath{\Phi}}{потенциал электрического поля, В}{false}
Подставляя \eqref{eq-eqfield-E_eq_gradPhi} в уравнения \eqref{eq-eqfield-divE_eq_rho}, \eqref{eq-eqfield-divE} получаем \textit{уравнение Пуассона}
\begin{equation}\label{eq-eqfield-Poisson_el}
\nabla^2 \gls{Phi} = \dpartfrac{^2\gls{Phi}}{x^2} +\dpartfrac{^2\gls{Phi}}{y^2}+\dpartfrac{^2\gls{Phi}}{z^2} = -\frac {\gls{rho}} {\gls{epsr}\gls{eps0}}.
\end{equation}

Получившееся выражение обозначают $ \Delta = \nabla^2 $ и называют оператором Лапласа. Для разных систем координат оператор Лапласа будет выглядеть по-разному.

 Если в рассматриваемой точке пространства отсутствуют объемные электрические заряды, то справедливо \textit{уравнение Лапласа}:
\begin{equation}\label{eq-eqfield-Laplace_el}
\nabla^2 \gls{Phi} = \dpartfrac{^2\gls{Phi}}{x^2} +\dpartfrac{^2\gls{Phi}}{y^2}+\dpartfrac{^2\gls{Phi}}{z^2} = 0.
\end{equation}
Подробнее см. учебник\footcite{Demirchyan2} \textsection 23.4, 24.4 .

Уравнения \eqref{eq-eqfield-Poisson_el} и \eqref{eq-eqfield-Laplace_el} справедливы для электростатического поля в однородной среде с постоянной относительной диэлектрической проницаемостью $ \gls{epsr} = \const $. Имеются формулы и в более общем виде, однако на данный момент нам достаточно такой формулировки.

Аналогичные уравнения справедливы для векторного потенциала магнитного поля. Вспомогательная величина -- векторный потенциал магнитного поля (обозначается буквой $ A $) -- специально введена в расчеты электромагнитного поля, чтобы унифицировать уравнения электрического и магнитного полей. 
 
\textit{Уравнение Пуассона} для магнитного поля выглядит как
\begin{equation}\label{eq-eqfield-Poisson-m}
\nabla^2 \vec{\gls{A}} = - \gls{mur} \gls{mu0} \vec{\gls{J}},
\end{equation}
где \newsym{A}{\ensuremath{A}}{векторный потенциал магнитного поля, Вб/м}{true}, \\
\newsym[z]{mur}{\ensuremath{\mu_r}}{относительная магнитная проницаемость}{true}, \\
\newsym[z]{mu0}{\ensuremath{\mu_0}}{магнитная постоянная, равная $ 4\pi \cdot 10^{-7} = 1.26\cdot 10^{-6} $ Гн/м}{true}, \\
\newsym{J}{\ensuremath{J}}{плотность электрического тока, А/м$ ^2 $}{true}.

\textit{Уравнение Лапласа} для магнитного поля выглядит аналогично электрическому полю.
\begin{equation}\label{eq-eqfield-Laplace-m}
\nabla^2 \vec{\gls{A}} = 0.
\end{equation}

Эти уравнения справедливы для постоянного магнитного поля в однородной среде с постоянной относительной магнитной проницаемостью $ \gls{mu0}=\const $.

Таким образом, мы получили выражение, которое из неизвестных величин содержит только потенциал моделируемого поля. Уравнения в каждой точке пространства представляет собой дифференциальные уравнения второго порядка в частных производных. На их численное решение и ориентированы компьютерные комплексы. 

Далее мы рассмотрим способы решения этих уравнений. А сейчас представим, что уравнения решены и нам известен потенциал поля. Тогда нам для расчета становятся доступными любые другие характеристики поля. 

Напряженность электрического поля находим согласно \eqref{eq-eqfield-E_eq_gradPhi}, электрическое смещение как $ \vec{\gls{D}}=\gls{epsr}\gls{eps0}\vec{\gls{E}} $, энергию электрического поля в каждой точке через 

\newsym{D}{\ensuremath{D}}{электрическое смещение, Кл/м$ ^{2} $}{false}

\begin{equation}
\gls{We}(x,y,z)=\dfrac{\gls{epsr}\gls{eps0} \gls{E}^2}{2},
\end{equation}
\newsym{We}{\ensuremath{W_\text{э}}}{энергия электрического поля, Дж}{false}
суммарную энергию электрического поля, как интеграл (сумму) энергий в каждой точке
\begin{equation}
\gls{We}=\int\limits_V \dfrac{\gls{epsr}\gls{eps0} \gls{E}^2}{2} dV.
\end{equation}
Можем найти емкость моделируемой системы, например конденсатора:
\begin{equation}
\gls{C} = \dfrac{\gls{q}}{\gls{U}}=\dfrac{2\gls{We}}{\gls{U}^2},
\end{equation}
где \newsym{C}{\ensuremath{C}}{электрическая емкость, Ф}{true}, 
\newsym{U}{\ensuremath{U}}{электрическое напряжение, В}{true}.

Для магнитного поля находим вектор магнитной индукции
\begin{equation}
\vec{\gls{B}} = \rot \vec{\gls{A}} = \nabla \times \vec{\gls{A}} = 
\begin{pmatrix}
	\dpartfrac{}{ x} \\[2ex]
	\dpartfrac{}{ y} \\[2ex]
	\dpartfrac{}{ z}
\end{pmatrix} 
\times \vec{\gls{A}} = 
\begin{vmatrix} 
	\Vec i & \Vec j & \Vec k \\[1ex]
	\dpartfrac{}{ x} & \dpartfrac{}{ y} & \dpartfrac{}{ z} \\[2ex]  
	\gls{A}_x & \gls{A}_y & \gls{A}_z 
\end{vmatrix}
\end{equation}
или
\begin{equation}\label{eq-eqfield-Bxyz}
\begin{aligned}
\gls{B}_x &= \dpartfrac{ \gls{A}_z}{ y} - \dpartfrac{ \gls{A}_y}{ z}, \\
\gls{B}_y &= \dpartfrac{ \gls{A}_x}{ z} - \dpartfrac{ \gls{A}_z}{ x}, \\
\gls{B}_z &= \dpartfrac{ \gls{A}_y}{ x} - \dpartfrac{ \gls{A}_x}{ y}.
\end{aligned}
\end{equation}

Через вектор магнитной индукции можем найти напряженность магнитного поля $ \vec{\gls{H}}=\vec{\gls{B}}/(\gls{mur}\gls{mu0}) $, потокосцепление \gls{Psi} сквозь $ n $ каких-либо поверхностей
\begin{equation}
\gls{Psi} = n \gls{upphi} = n\iint\limits_S \Vec{\gls{B}}\cdot {\rm d}\Vec S = n \oint\limits_l \vec{\gls{A}} \cdot \Vec dl.
\end{equation}
\newsym[z]{Psi}{\ensuremath{\Psi}}{потокосцепление, Вб}{false}
\newsym{H}{\ensuremath{H}}{напряженность магнитного поля, А/м}{false}

Можно рассчитать энергию магнитного поля в каждой точке
\begin{equation}
\gls{Wm}(x,y,z) = \dfrac{ \gls{B}^2}{2\gls{mur}\gls{mu0}}
\end{equation}\newsym{Wm}{\ensuremath{W_\text{м}}}{энергия магнитного поля, Дж}{false}
 и суммарную энергию магнитного поля
\begin{equation}
\gls{Wm} = \int\limits_V \dfrac{ \gls{B}^2}{2\gls{mur}\gls{mu0}} dV.
\end{equation}

Индуктивность исследуемого объекта, например катушки индуктивности, вычислим с помощью формулы
\begin{equation}
\gls{L} = \dfrac{\gls{Psi}}{\gls{I}}=\dfrac{2\gls{Wm}}{\gls{I}^2},
\end{equation}
где \newsym{L}{\ensuremath{L}}{индуктивность, Гн}{true}, 
\newsym{I}{\ensuremath{I}}{электрический ток, А}{true}.

И так далее, получив решение относительно потенциала электрического поля или магнитного поля, можно восстановить любые требуемые нам характеристики поля.

\textbf{Подведем итог.} В компьютерных технологиях расчета и моделирования электромагнитных полей используются уравнения Пуассона и Лапласа:
\begin{equation}\label{eq-eqfield-Laplace-Poisson}
\nabla^2 \gls{P} = -f, \quad \nabla^2 \gls{P} = 0.
\end{equation}
где обозначение \newsym{P}{\ensuremath{P}}{потенциал любого (электрического или магнитного) поля}{true}, а $ f $ -- его источник. Оператор лапласа $ \nabla^2 $ в декартовых координатах записывается как
\begin{equation}
\nabla^2 \gls{P} = \dpartfrac{^2\gls{P}}{x^2} +\dpartfrac{^2\gls{P}}{y^2}+\dpartfrac{^2\gls{P}}{z^2}.
\end{equation}
Запись оператора Лапласа в других системах координат приведена в приложении \ref{appendix_formulas}.
